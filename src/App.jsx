import React from "react";
import { 
    Typography, 
    AppBar, 
    Card, 
    CardActions, 
    CardContent, 
    CardMedia, 
    CssBaseline, 
    Grid, 
    Toolbar, 
    Container,
    Paper,
    List,
    ListItem,
    ListItemAvatar,
    ListItemText,
    Divider,
    Avatar,
    TextField,
    FormControl
} from "@mui/material";
import SendIcon from '@mui/icons-material/Send';
import { Box } from "@mui/system";
const App = () => {
    return (
        <>
            <CssBaseline />
            <AppBar position="relative">
                <Toolbar>
                    <Typography variant="h6">
                        Hello World Chat Meta World
                    </Typography>
                </Toolbar>
            </AppBar>
            <main>
                <div>
                    <Container maxWidth="sm">
                        <Typography variant="h2" align="center" color="textPrimary" gutterButtom>
                            Chat Room
                        </Typography>
                        <Paper variant="outlined" sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}>
                            <List sx={{ width: '100%', bgcolor: 'background.paper', pb: {md: 10 } }}>
                                <ListItem alignItems="flex-start">
                                    <ListItemAvatar>
                                        <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary="Brunch this weekend?"
                                        secondary={
                                        <React.Fragment>
                                            <Typography
                                            sx={{ display: 'inline' }}
                                            component="span"
                                            variant="body2"
                                            color="text.primary"
                                            >
                                            Ali Connors
                                            </Typography>
                                            {" — I'll be in your neighborhood doing errands this…"}
                                        </React.Fragment>
                                        }
                                    />
                                </ListItem>
                            </List>

                            <Box>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} sm={10}>
                                        <TextField
                                            placeholder="Write message ..."
                                            variant="outlined"
                                            multiline
                                            autoFocus
                                            fullWidth
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={2}>
                                        <SendIcon />    
                                    </Grid>
                                </Grid>
                                
                            </Box>
                            
                        </Paper>
                    </Container>
                </div>
            </main>
        </>
    );
}

export default App;